import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Axios from 'axios';
import Scream from '../components/Scream/Scream';

class Home extends Component {
  state = {
    screams: null
  };
  componentDidMount() {
    Axios.get('/screams')
      .then(res => {
        this.setState({
          screams: res.data
        });
      })
      .catch(err => console.log(err));
  }
  render() {
    const { screams } = this.state;
    return (
      <Grid container spacing={16}>
        <Grid item sm={8} xs={12}>
          {screams && screams.length > 0 ? (
            screams.map(scream => {
              return <Scream scream={scream} />;
            })
          ) : (
            <p>Loading...</p>
          )}
        </Grid>
        <Grid item sm={4} xs={12}>
          <p>bye</p>
        </Grid>
      </Grid>
    );
  }
}

export default Home;
