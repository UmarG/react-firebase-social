import React from 'react';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    display: 'flex',
    marginBottom: 20
  },
  image: {
    minWidth: 200
  },
  content: {
    padding: 25,
    objectFit: 'cover'
  }
};

const Scream = ({ classes, scream }) => {
  return (
    <Card className={classes.card}>
      {scream.userImage && (
        <CardMedia
          src={scream.userImage}
          title="Profile Image"
          className={classes.image}
        />
      )}
      <CardContent className={classes.content}>
        <Typography
          variant="h5"
          component={Link}
          to={`/users/${scream.userHandle}`}
          color="primary"
        >
          {scream.userHandle}
        </Typography>
        <Typography variant="body2" color="textSecondary">
          {scream.createdAt}
        </Typography>
        <Typography variant="body1">{scream.body}</Typography>
      </CardContent>
    </Card>
  );
};

export default withStyles(styles)(Scream);
